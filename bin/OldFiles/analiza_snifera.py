import json
import os

def analyse_log(logPath, exec_numMotes):

    tx_packets = {}
    sniffed_packets = {}
    tx_num = 0
    sniffed_num = 0
    detected_packets = 0
    undetected_packets = 0
    multiple_detected = 0
    uniquely_detected = 0
    average_pdr = 0

    log_filename = logPath+'\exec_numMotes_'+str(exec_numMotes)+'.dat'
    with open(log_filename, 'r') as inf:
        for line in inf:
            if '"tsch.txdone"' in line:
                s = line
                json_acceptable_string = s.replace("'", "\"")
                d = json.loads(json_acceptable_string)
                tx_packets[tx_num] = d
                tx_num += 1
            elif '"sniffer.rxdone"' in line:
                s = line
                json_acceptable_string = s.replace("'", "\"")
                d = json.loads(json_acceptable_string)
                sniffed_packets[sniffed_num] = d
                sniffed_num += 1
            elif '"sniffer.selection"' in line:
                s = line
                json_acceptable_string = s.replace("'", "\"")
                d = json.loads(json_acceptable_string)
                average_pdr = d[u'average_pdr']

    for tx_pck in tx_packets:
        match = 0
        for snf_pck in sniffed_packets:
            if (tx_packets[tx_pck][u'_asn'] == sniffed_packets[snf_pck][u'_asn'] and
            tx_packets[tx_pck][u'packet'] == sniffed_packets[snf_pck][u'packet']):
                match += 1

        if match >= 1:
            detected_packets += 1
            if match == 1:
                uniquely_detected += 1
            elif match > 1:
                multiple_detected += 1
        elif match == 0:
            undetected_packets += 1

    result_filename = 'simData\sniffer_analysis'+str(logPath).replace('simData','')+'.txt'

    with open(result_filename, 'a') as f:
        f.write(str(logPath).replace('simData','')+'# average_pdr: '+str(average_pdr)+' %det: '+str(detected_packets/(detected_packets+undetected_packets)*100)+' tx_num: '+str(tx_num)+' sniffed_num: '+str(sniffed_num)+' detected_packets: '+str(detected_packets)+' undetected_packets: '+str(undetected_packets)+' multiple_detected: '+str(multiple_detected)+' uniquely_detected: '+str(uniquely_detected))
