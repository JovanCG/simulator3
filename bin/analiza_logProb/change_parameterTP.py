import json
import sys


def change_parametar():

    target_pdr = float(sys.argv[1])

    with open('parameter.json', 'r+') as f:
        data = json.load(f)
        data['target_pdr'] = target_pdr

    with open('parameter.json', 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    change_parametar()
