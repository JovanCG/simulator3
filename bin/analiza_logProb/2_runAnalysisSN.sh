#!/bin/sh

#SBATCH --mail-type=ALL
#SBATCH --mail-user=jovan.crnogorac21@gmail.com

source /home/jcrnogor/.bashrc
conda activate py38

python 3_runAnalysisSN.py
