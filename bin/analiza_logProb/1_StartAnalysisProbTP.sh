#!/bin/sh

source /home/jcrnogor/.bashrc
conda activate py38

for target_pdr in `seq 0.1 0.05 1.0`
do
    python change_parameterTP.py $target_pdr
    sbatch "2_runAnalysisTP.sh"
    sleep 10
done
