import sniffer_analysisNEWTP
import json

with open('parameter.json') as json_file:
    data = json.load(json_file)
    target_pdr = data['target_pdr']

sniffer_analysisNEWTP.analyse_log(logPath='simData/sve', exec_numMotes=50, target_pdr=target_pdr)
