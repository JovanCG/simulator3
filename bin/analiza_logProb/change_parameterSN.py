import json
import sys


def change_parameter():

    sniffer_num = float(sys.argv[1])

    with open('parameter.json', 'r+') as f:
        data = json.load(f)
        data['sniffer_num'] = sniffer_num

    with open('parameter.json', 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    change_parameter()
