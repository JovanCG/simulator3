import sniffer_analysisNEWSN
import json

with open('parameter.json') as json_file:
    data = json.load(json_file)
    sniffer_num = data['sniffer_num']

sniffer_analysisNEWSN.analyse_log(logPath='simData/sve', exec_numMotes=50, sniffer_num=sniffer_num)
