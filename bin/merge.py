import glob
import os
import json
import shutil

def merge_output_files(folder_path):
    """
    Read the dataset folders and merge the datasets (usefull when using multiple CPUs).
    :param string folder_path:
    """
    filenames = []
    log_name = folder_path+'.dat'
    for fajl in os.listdir(folder_path):
        filenames.append(folder_path+'/'+fajl)


    with open(log_name, 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)
            os.remove(fname)


if __name__ == '__main__':
    # pusta se iz bin foldera i spaja puknute fajlove
    for subfolder in os.listdir('simData/Graph/sve'):
        if not os.path.isfile('simData/sve/'+subfolder):
            print('simData/sve/'+subfolder)
            merge_output_files('simData/sve/'+subfolder)
