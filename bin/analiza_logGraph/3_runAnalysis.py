import sniffer_analysisNEW
import json

with open('parameter.json') as json_file:
    data = json.load(json_file)
    sniffer_link_pdr = data['sniffer_link_pdr']
    removal_load = data['removal_load']

sniffer_analysisNEW.analyse_log(logPath='simData/sve', exec_numMotes=50, sniffer_link_pdr=sniffer_link_pdr, removal_load=removal_load)
