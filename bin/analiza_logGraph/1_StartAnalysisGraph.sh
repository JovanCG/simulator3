#!/bin/sh

source /home/jcrnogor/.bashrc
conda activate py38

for sniffer_link_pdr in `seq 0.1 0.1 1.0`
do
    for remove_load in `seq 0.0 0.1 1.0`
    do 
        python change_parameter.py $sniffer_link_pdr $remove_load
        sbatch "2_runAnalysis.sh"
        sleep 10
    done
done
