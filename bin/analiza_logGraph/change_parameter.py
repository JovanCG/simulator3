import json
import sys


def change_parameter():

    sniffer_link_pdr = float(sys.argv[1])
    removal_load = float(sys.argv[2])

    with open('parameter.json', 'r+') as f:
        data = json.load(f)
        data['removal_load'] = removal_load  # % of remove
        data['sniffer_link_pdr'] = sniffer_link_pdr #threshold

    with open('parameter.json', 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    change_parameter()
