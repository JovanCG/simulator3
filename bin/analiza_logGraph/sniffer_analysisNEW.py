import json
import os
import gzip
import shutil


def analyse_log(logPath, exec_numMotes, sniffer_link_pdr, removal_load):

    tx_packets_asn      = {}
    sniffed_packets_asn = {}
    tx_num_asn        = 0
    sniffed_num_asn   = 0
    tx_num_run        = 0
    sniffed_num_run   = 0
    average_pdr       = 0
    current_asn     = 0
    past_asns       = []
    detected_packets_run   = 0
    undetected_packets_run = 0
    multiple_detected_run  = 0
    uniquely_detected_run  = 0
    num_runs = 0

    first_run       = True

    log_filename = logPath+'/exec_numMotes_'+str(exec_numMotes)+'_sniffer_link_pdr_'+str(sniffer_link_pdr)+'_removal_load_'+str(removal_load)+'.dat' #cleps

    # otvaranje log fajla
    if os.path.isfile(log_filename):
        with open(log_filename, 'r') as inf:
            # petlja kroz sve linije log fajla
            for line in inf:
                if '"cpuID"' in line:
                    # pocinje novi Run
                    s = line
                    json_acceptable_string = s.replace("'", "\"")
                    d = json.loads(json_acceptable_string)
                    sniffer_link_pdr = d[u'sniffer_link_pdr']
                    removal_load = d[u'removal_load']


                    if not first_run:
                        # dosli smo do novog run-a, logujemo prethodne rezultate


                        sniffer_link_pdr = d[u'sniffer_link_pdr']
                        removal_load = d[u'removal_load']

                        # analiziraj poslednji asn prethodnog run-a
                        if (tx_packets_asn != {} or sniffed_packets_asn != {}):

                            detected_packets_asn, undetected_packets_asn, multiple_detected_asn, uniquely_detected_asn = analyse_asn_data(tx_packets_asn=tx_packets_asn, sniffed_packets_asn=sniffed_packets_asn)

                            detected_packets_run   += detected_packets_asn
                            undetected_packets_run += undetected_packets_asn
                            multiple_detected_run  += multiple_detected_asn
                            uniquely_detected_run  += uniquely_detected_asn
                            tx_num_run             += tx_num_asn
                            sniffed_num_run        += sniffed_num_asn

                        num_runs += 1

                        log_data(logPath=logPath, sniffer_link_pdr=sniffer_link_pdr, removal_load=removal_load, detected_packets_run=detected_packets_run, undetected_packets_run=undetected_packets_run, multiple_detected_run=multiple_detected_run, uniquely_detected_run=uniquely_detected_run, sniffers_locations=sniffers_locations, average_pdr=average_pdr, tx_num_run=tx_num_run, sniffed_num_run=sniffed_num_run, num_runs=num_runs, current_asn=current_asn)

                        # resetuj promjenljive za sledeci run
                        tx_packets_asn      = {}
                        sniffed_packets_asn = {}
                        tx_num_asn = 0
                        sniffed_num_asn = 0

                        average_pdr     = 0
                        current_asn     = 0
                        past_asns       = []

                        tx_num_run        = 0
                        sniffed_num_run   = 0
                        detected_packets_run   = 0
                        undetected_packets_run = 0
                        multiple_detected_run  = 0
                        uniquely_detected_run  = 0

                    else:
                        # radi se o prvom run-u nema prethodnih podataka
                        first_run = False
                else:
                    s = line
                    json_acceptable_string = s.replace("'", "\"")
                    d = json.loads(json_acceptable_string)

                    # provjera asn-a
                    if current_asn != d[u'_asn']:
                        # novi asn ne smije biti ranije analiziran
                        assert d[u'_asn'] not in past_asns
                        past_asns.append(d[u'_asn'])

                        # podesi asn
                        current_asn = d[u'_asn']

                        # poceo je novi asn, uradi analizu za stari
                        if tx_packets_asn != {} or sniffed_packets_asn != {}:

                            detected_packets_asn, undetected_packets_asn, multiple_detected_asn, uniquely_detected_asn = analyse_asn_data(tx_packets_asn=tx_packets_asn, sniffed_packets_asn=sniffed_packets_asn)

                            detected_packets_run   += detected_packets_asn
                            undetected_packets_run += undetected_packets_asn
                            multiple_detected_run  += multiple_detected_asn
                            uniquely_detected_run  += uniquely_detected_asn
                            tx_num_run             += tx_num_asn
                            sniffed_num_run        += sniffed_num_asn

                        # isprazni promjenljive za asn
                        tx_packets_asn = {}
                        sniffed_packets_asn = {}
                        tx_num_asn = 0
                        sniffed_num_asn = 0

                    # prikupi sve poslate pakete u ovom asn-u
                    if '"tsch.txdone"' in line:
                        tx_packets_asn[tx_num_asn] = d
                        tx_num_asn += 1

                    # prikupi sve snifovane pakete
                    elif '"sniffer.rxdone"' in line:
                        sniffed_packets_asn[sniffed_num_asn] = d
                        sniffed_num_asn += 1

                    # preuzmi podatke o odabranim sniferima
                    elif '"sniffer.selection"' in line:
                        sniffers_locations = d[u'sniffers_locations']
                        average_pdr = d[u'average_pdr']

        # analiziraj poslednji asn poslednjeg run-a
        if tx_packets_asn != {} or sniffed_packets_asn != {}:
            detected_packets_asn, undetected_packets_asn, multiple_detected_asn, uniquely_detected_asn = analyse_asn_data(tx_packets_asn=tx_packets_asn, sniffed_packets_asn=sniffed_packets_asn)

            detected_packets_run   += detected_packets_asn
            undetected_packets_run += undetected_packets_asn
            multiple_detected_run  += multiple_detected_asn
            uniquely_detected_run  += uniquely_detected_asn
            tx_num_run             += tx_num_asn
            sniffed_num_run        += sniffed_num_asn

        # loguj rezultate poslednjeg run-a
        num_runs += 1

        log_data(logPath=logPath, sniffer_link_pdr=sniffer_link_pdr, removal_load=removal_load, detected_packets_run=detected_packets_run, undetected_packets_run=undetected_packets_run, multiple_detected_run=multiple_detected_run, uniquely_detected_run=uniquely_detected_run, sniffers_locations=sniffers_locations, average_pdr=average_pdr, tx_num_run=tx_num_run, sniffed_num_run=sniffed_num_run, num_runs=num_runs, current_asn=current_asn)

        # zipuj i brisi veliki fajl
        gzip_filename = log_filename+'.gz'

        with open(log_filename, 'rb') as f_in, gzip.open(gzip_filename, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        os.remove(log_filename)


    else:    ## Show an error ##
        print("Error: %s file not found" % log_filename)

def analyse_asn_data(tx_packets_asn, sniffed_packets_asn):

    detected_packets_asn   = 0
    undetected_packets_asn = 0
    multiple_detected_asn  = 0
    uniquely_detected_asn  = 0

    for tx_pck in tx_packets_asn:
        match = 0
        for snf_pck in sniffed_packets_asn:
            if tx_packets_asn[tx_pck][u'packet'] == sniffed_packets_asn[snf_pck][u'packet']:
                match += 1
            # paket je detektovan vise od jednog puta, nije bitno koliko puta vise
            if match > 1:
                break

        if match >= 1:
            detected_packets_asn += 1
            if match == 1:
                uniquely_detected_asn += 1
            elif match > 1:
                multiple_detected_asn += 1
        elif match == 0:
            undetected_packets_asn += 1


    assert detected_packets_asn == (uniquely_detected_asn+multiple_detected_asn)
    assert len(tx_packets_asn) == (detected_packets_asn+undetected_packets_asn)

    return detected_packets_asn, undetected_packets_asn, multiple_detected_asn, uniquely_detected_asn

def log_data(logPath, sniffer_link_pdr, removal_load, detected_packets_run, undetected_packets_run, multiple_detected_run, uniquely_detected_run, sniffers_locations, average_pdr, tx_num_run, sniffed_num_run, num_runs, current_asn):

    result_filename = logPath+'/sniffer_link_pdr_'+str(sniffer_link_pdr)+'_removal_load_'+str(removal_load)+'.txt'

    # sacuvaj txt log
    with open(result_filename, 'a') as f:
        if current_asn > 500000:
            if detected_packets_run > 0 and undetected_packets_run > 0:
                f.write('{"ASN": ' + str(current_asn) + ', ' + '"CPU": ' + '"' + str(logPath).replace('simData','') + '", ' + '"RunNo": ' + str(num_runs) + ', ' + '"sniffer_link_pdr": ' + str(sniffer_link_pdr) + ', ' + '"removal_load": ' + str(removal_load) + ', ' + '"sniffer_num": ' + str(len(sniffers_locations)) + ', ' + '"average_pdr": ' +str(average_pdr.__round__(2)) + ', ' + '"%det": ' + str((detected_packets_run/(detected_packets_run+undetected_packets_run)*100).__round__(2)) + ', ' + '"tx_num": ' + str(tx_num_run) + ', ' + '"sniffed_num": ' + str(sniffed_num_run) + ', ' + '"detected_packets": ' + str(detected_packets_run) + ', ' + '"undetected_packets": ' + str(undetected_packets_run) + ', ' + '"multiple_detected": ' + str(multiple_detected_run) + ', ' + '"uniquely_detected": ' + str(uniquely_detected_run) + '}' + '\n')
            else:
                f.write('{"ASN": ' + str(current_asn) + ', ' + '"CPU": ' + '"' + str(logPath).replace('simData','') + '", ' + '"RunNo": ' + str(num_runs) + ', ' + '"sniffer_link_pdr": ' + str(sniffer_link_pdr) + ', ' + '"removal_load": ' + str(removal_load) + ', ' + '"sniffer_num": ' + str(len(sniffers_locations)) + ', ' + '"average_pdr": ' +str(average_pdr.__round__(2)) + ', ' + '"%det": ' + str(0) + ', ' + '"tx_num": ' + str(tx_num_run) + ', ' + '"sniffed_num": ' + str(sniffed_num_run) + ', ' + '"detected_packets": ' + str(detected_packets_run) + ', ' + '"undetected_packets": ' + str(undetected_packets_run) + ', ' + '"multiple_detected": ' + str(multiple_detected_run) + ', ' + '"uniquely_detected": ' + str(uniquely_detected_run) + '}' + '\n')
        else:
            if detected_packets_run > 0 and undetected_packets_run > 0:
                f.write('{"ASN": ' + '"!'+str(current_asn)+'!"' + ', ' + '"CPU": ' + '"' + str(logPath).replace('simData','') + '", ' + '"RunNo": ' + str(num_runs) + ', ' + '"sniffer_link_pdr": ' + str(sniffer_link_pdr) + ', ' + '"removal_load": ' + str(removal_load) + ', ' + '"sniffer_num": ' + str(len(sniffers_locations)) + ', ' + '"average_pdr": ' +str(average_pdr.__round__(2)) + ', ' + '"%det": ' + str((detected_packets_run/(detected_packets_run+undetected_packets_run)*100).__round__(2)) + ', ' + '"tx_num": ' + str(tx_num_run) + ', ' + '"sniffed_num": ' + str(sniffed_num_run) + ', ' + '"detected_packets": ' + str(detected_packets_run) + ', ' + '"undetected_packets": ' + str(undetected_packets_run) + ', ' + '"multiple_detected": ' + str(multiple_detected_run) + ', ' + '"uniquely_detected": ' + str(uniquely_detected_run) + '}' + '\n')
            else:
                f.write('{"ASN": ' + '"!'+str(current_asn)+'!"' + ', ' + '"CPU": ' + '"' + str(logPath).replace('simData','') + '", ' + '"RunNo": ' + str(num_runs) + ', ' + '"sniffer_link_pdr": ' + str(sniffer_link_pdr) + ', ' + '"removal_load": ' + str(removal_load) + ', ' + '"sniffer_num": ' + str(len(sniffers_locations)) + ', ' + '"average_pdr": ' +str(average_pdr.__round__(2)) + ', ' + '"%det": ' + str(0) + ', ' + '"tx_num": ' + str(tx_num_run) + ', ' + '"sniffed_num": ' + str(sniffed_num_run) + ', ' + '"detected_packets": ' + str(detected_packets_run) + ', ' + '"undetected_packets": ' + str(undetected_packets_run) + ', ' + '"multiple_detected": ' + str(multiple_detected_run) + ', ' + '"uniquely_detected": ' + str(uniquely_detected_run) + '}' + '\n')
