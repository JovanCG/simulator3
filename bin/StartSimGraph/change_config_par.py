import json
import sys


def change_cfg_par():

    sniffer_link_pdr = float(sys.argv[1])
    removal_load = float(sys.argv[2])

    with open('../config.json', 'r+') as f:
        data = json.load(f)
        data['settings']['combination']['removal_load'] = [removal_load]  # % of remove
        data['settings']['combination']['sniffer_link_pdr'] = [sniffer_link_pdr] #threshold

    with open('../config.json', 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    change_cfg_par()
