#!/bin/sh

source /home/jcrnogor/.bashrc
conda activate py38


python change_config_par.py $0.1 $0.3
sbatch "2_runSim.sh"
sleep 10

python change_config_par.py $0.1 $0.8
sbatch "2_runSim.sh"
sleep 10

python change_config_par.py $0.2 $0.3
sbatch "2_runSim.sh"
sleep 10
