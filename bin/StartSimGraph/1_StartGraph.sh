#!/bin/sh

source /home/jcrnogor/.bashrc
conda activate py38

for sniffer_link_pdr in `seq 0.1 0.1 1.0`
do
    for removal_load in `seq 0.0 0.1 1.0`
    do 
        python change_config_par.py $sniffer_link_pdr $removal_load
        sbatch "2_runSim.sh"
        sleep 10
    done
done

