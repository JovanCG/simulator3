import json
import sys


def change_paameterTP():

    target_pdr = float(sys.argv[1])

    with open('config.json', 'r+') as f:
        data = json.load(f)
        data['settings']['combination']['target_pdr'] = [target_pdr]  # % target_pdr

    with open('config.json', 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    change_paameterTP()
