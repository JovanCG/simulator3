#!/bin/sh

source /home/jcrnogor/.bashrc
conda activate py38

for sniffer_num in `seq 1 1 15`
do
    python change_parameterSN.py $sniffer_num
    sbatch "2_runSim.sh"
    sleep 10
done
