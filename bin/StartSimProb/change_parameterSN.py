import json
import sys


def change_parameterSN():

    sniffer_num = float(sys.argv[1])

    with open('config.json', 'r+') as f:
        data = json.load(f)
        data['settings']['combination']['sniffer_num'] = [sniffer_num]  # % sniffer_num

    with open('config.json', 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    change_parameterSN()
