import glob
import os
import json
import shutil
import sys
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches

xG = []
yG = []
xP = []
yP = []

def analyze_graph_file(folder_path, file_name):


    avg_sniffer_num = 0
    avg_average_pdr = 0
    avg_det         = 0
    num_runs        = 0
    det_pcks        = 0
    mult_det        = 0
    uniq_det        = 0
    und_det         = 0
    tx_num          = 0
    target_pdr = 0
    A = []
    B = []
    C = []


    with open(folder_path, 'r') as log_file:
        for line in log_file:
            num_runs += 1
            linija = line.split()

            avg_sniffer_num += int(linija[7])
            avg_average_pdr += float(linija[9])
            avg_det         += float(linija[11])

            sniffer_link_pdr = float(linija[3])
            removal_load     = float(linija[5])

    avg_sniffer_num = round(avg_sniffer_num/num_runs,2)
    avg_average_pdr = round(avg_average_pdr/num_runs,2)
    avg_det         = round(avg_det/num_runs,2)

    xG.append(avg_det)
    yG.append(avg_sniffer_num)

    print('target_pdr', target_pdr, 'reached_pdr',  avg_average_pdr, 'real%det', avg_det,'sniffer_num', avg_sniffer_num)

def analyze_prob_file(folder_path, file_name):


    avg_sniffer_num = 0
    avg_average_pdr = 0
    avg_det         = 0
    num_runs        = 0
    det_pcks        = 0
    mult_det        = 0
    uniq_det        = 0
    und_det         = 0
    tx_num          = 0
    target_pdr = 0
    A = []
    B = []
    C = []


    with open(folder_path, 'r') as log_file:
        for line in log_file:
            num_runs += 1
            ldict = json.loads(line)

            # preskoci neuspjesne redove
            if not str(ldict['ASN']).find('!'):
                print(ldict["ASN"])
                continue

            target_pdr = ldict['target_pdr']

            avg_sniffer_num += ldict['sniffer_num']
            avg_average_pdr += ldict['average_pdr']
            avg_det         += ldict['%det']

    avg_sniffer_num = round(avg_sniffer_num/num_runs,4)
    avg_average_pdr = round(avg_average_pdr/num_runs,4)
    avg_det         = round(avg_det/num_runs,4)

    xP.append(avg_det)
    yP.append(avg_sniffer_num)

    print('target_pdr', target_pdr, 'reached_pdr',  avg_average_pdr, 'real%det', avg_det,'sniffer_num', avg_sniffer_num)


if __name__ == '__main__':
    # pusta se iz bin foldera i spaja puknute fajlove
    for subfolder in os.listdir('../simData/Graph/Random/'):
        if os.path.isfile('../simData/Graph/Random/'+subfolder):
            analyze_graph_file('../simData/Graph/Random/'+subfolder, subfolder)

    for subfolder in os.listdir('../simData/Prob/Random/'):
        if os.path.isfile('../simData/Prob/Random/'+subfolder):
            analyze_prob_file('../simData/Prob/Random/'+subfolder, subfolder)

    print(xG)
    print(yG)
    print(len(xG),len(yG))
    plt.grid(linestyle = '--', linewidth = 0.5)
    plt.plot(xG, yG, 'x', color='red')
    plt.ylim(0,52)
    plt.yticks([0,5,10,15,20,25,30,35,40,45,50])
    plt.xticks([0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100])
    plt.xlabel('Detected packets (%)', loc = 'right')
    plt.ylabel('Number of sniffers', loc = 'top')
    red_patch = mpatches.Patch(color='red', label='Graph theory')
    blue_patch = mpatches.Patch(color='blue', label='Probabilistic theory')
    plt.legend(handles=[red_patch, blue_patch])

    plt.plot(xP, yP, 'x', color='blue')

    plt.show()
