import glob
import os
import json
import shutil
import sys
import matplotlib.pyplot as plt

x = []
y = []

def analyze_txt_file(folder_path, file_name):


    avg_sniffer_num = 0
    avg_average_pdr = 0
    avg_det         = 0
    num_runs        = 0
    det_pcks        = 0
    mult_det        = 0
    uniq_det        = 0
    und_det         = 0
    tx_num          = 0
    sniffer_link_pdr = 0
    removal_load    = 0
    A = []
    B = []
    C = []


    with open(folder_path, 'r') as log_file:
        for line in log_file:
            num_runs += 1
            linija = line.split()
            print(linija)
            avg_sniffer_num += int(linija[7])
            avg_average_pdr += float(linija[9])
            avg_det         += float(linija[11])

            sniffer_link_pdr = float(linija[3])
            removal_load     = float(linija[5])

            # Grafik 2
            '''
            tx_num   += int(linija[13])
            det_pcks += int(linija[17])
            und_det  += int(linija[19])
            uniq_det += int(linija[23])
            mult_det += int(linija[21])
            '''



    avg_sniffer_num = round(avg_sniffer_num/num_runs,2)
    avg_average_pdr = round(avg_average_pdr/num_runs,2)
    avg_det         = round(avg_det/num_runs,2)

    x.append(avg_det)
    y.append(avg_sniffer_num)

    print('sniffer_link_pdr', sniffer_link_pdr, 'removal_load', removal_load, 'reached_pdr',  avg_average_pdr, 'real%det', avg_det,'sniffer_num', avg_sniffer_num)


    # Grafik 2
    '''
    assert det_pcks == uniq_det + mult_det
    assert det_pcks+und_det == tx_num
    multi = round(mult_det/det_pcks*avg_det,2)
    unique = round(uniq_det/det_pcks*avg_det,2)
    '''

    #print('slp', sniffer_link_pdr, 'rem', removal_load, 'avg_sniffer_num', avg_sniffer_num, 'avg_average_pdr', avg_average_pdr, 'avg_det', avg_det, 'M', multi, 'U', unique)

    # Grafik 2
    '''
    if sniffer_link_pdr in [0.1,0.3,0.5,0.7,0.9] and removal_load in [0.3,0.5,0.7]:
        print('slp', sniffer_link_pdr, 'rem', removal_load, 'avg_det', avg_det, 'M', multi, 'U', unique)
    '''

    sys.exit()



if __name__ == '__main__':
    # pusta se iz bin foldera i spaja puknute fajlove
    for subfolder in os.listdir('simData/Graph/random'):
        if os.path.isfile('simData/Graph/random'+subfolder):
            #print('simData/random/'+subfolder)
            analyze_txt_file('simData/Graph/random'+subfolder, subfolder)
    print(x)
    print(y)
    plt.plot(x, y, 'x-', color='red')
    plt.show()
