import glob
import os
import json
import shutil
import sys
import matplotlib.pyplot as plt

x = []
y = []

def analyze_txt_file(folder_path, file_name):


    avg_sniffer_num = 0
    avg_average_pdr = 0
    avg_det         = 0
    num_runs        = 0
    det_pcks        = 0
    mult_det        = 0
    uniq_det        = 0
    und_det         = 0
    tx_num          = 0
    target_pdr = 0
    A = []
    B = []
    C = []


    with open(folder_path, 'r') as log_file:
        for line in log_file:
            num_runs += 1
            ldict = json.loads(line)

            target_pdr = ldict['target_pdr']

            avg_sniffer_num += ldict['sniffer_num']
            avg_average_pdr += ldict['average_pdr']
            avg_det         += ldict['%det']

    avg_sniffer_num = round(avg_sniffer_num/num_runs,2)
    avg_average_pdr = round(avg_average_pdr/num_runs,2)
    avg_det         = round(avg_det/num_runs,2)

    x.append(avg_det)
    y.append(avg_sniffer_num)

    print('target_pdr', target_pdr, 'reached_pdr',  avg_average_pdr, 'real%det', avg_det,'sniffer_num', avg_sniffer_num)



if __name__ == '__main__':
    # pusta se iz bin foldera i spaja puknute fajlove
    for subfolder in os.listdir('simData/Prob/Random/'):
        if os.path.isfile('simData/Prob/Random/'+subfolder):
            #print('simData/random/'+subfolder)
            analyze_txt_file('simData/Prob/Random/'+subfolder, subfolder)

    print(x)
    print(y)
    plt.plot(x, y, 'x-', color='red')
    plt.show()
