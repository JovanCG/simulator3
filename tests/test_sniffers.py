import pytest

from . import test_utils as u
import SimEngine.Mote.MoteDefines as d
from SimEngine import SimLog
import networkx as nx
import os

#=== test for ConnectivityRandom
class TestRandom(object):

    def test_sniffer_position(self, sim_engine):
        sim_engine = sim_engine(
            diff_config = {
                'exec_numMotes'                 : 50,
                'exec_numSlotframesPerRun'      : 1,
                'conn_class'                    : 'Random',
                "conn_random_init_min_neighbors": 3,
                "phy_numChans"                  : 16,
            }
        )

        motes  = sim_engine.motes
        matrix = sim_engine.connectivity.matrix

        for mote in motes:
            if mote.onMote != -1:
                assert matrix.coordinates[mote.id] == matrix.coordinates[mote.onMote]

   #provjera da li uredjaj i snifer koji se nalazi pored njega imaju iste podesene linkove
    def test_sniffer_links(self, sim_engine):

        num_channels = 16
        sim_engine = sim_engine(
            diff_config = {
                'exec_numMotes'                 : 50,
                'exec_numSlotframesPerRun'      : 1,
                'conn_class'                    : 'Random',
                'conn_random_init_min_neighbors': 3,
                'phy_numChans'                  : num_channels
            }
        )

        motes  = sim_engine.motes
        matrix = sim_engine.connectivity.matrix

        for mote in motes:
            if mote.onMote != -1: #posmatramo iz ugla snifera
                for node in motes:
                    if mote.onMote != node.id and node.onMote == -1:
                        #nije snifer, nije uredjaj na kojem se snifer nalazi

                        for channel in d.TSCH_HOPPING_SEQUENCE[:num_channels]:
                            assert matrix._matrix[mote.id][node.id][channel] == matrix._matrix[mote.onMote][node.id][channel]
                            assert matrix._matrix[node.id][mote.id][channel] == matrix._matrix[node.id][mote.onMote][channel]

                    elif mote.onMote == node.id:
                        #uredjaj na kojem se snifer nalazi
                        for channel in d.TSCH_HOPPING_SEQUENCE[:num_channels]:
                            assert matrix.get_pdr(mote.id,
                                                  node.id,
                                                  channel) > 0.9
                            assert matrix.get_pdr(node.id,
                                                  mote.id,
                                                  channel) > 0.9
                            #rssi
                            assert matrix.get_rssi(mote.id,
                                                  node.id,
                                                  channel) > -88
                            assert matrix.get_rssi(node.id,
                                                  mote.id,
                                                  channel) > -88
                    elif node.onMote != -1:
                        #sniferi (on isti, drugi sniferi)
                        for channel in d.TSCH_HOPPING_SEQUENCE[:num_channels]:
                            assert matrix._matrix[mote.id][node.id][channel] == {u'pdr': 0, u'rssi': -1000}

    def test_sniffer_location(self, sim_engine):

        num_channels = 16
        sim_engine = sim_engine(
            diff_config = {
                'exec_numMotes'                 : 50,
                'exec_numSlotframesPerRun'      : 1,
                'conn_class'                    : 'Random',
                "conn_random_init_min_neighbors": 3,
                "phy_numChans"                  : num_channels,
            }
        )

        motes  = sim_engine.motes
        matrix = sim_engine.connectivity.matrix

        #print(matrix.coordinates.items())
        cord_dict  = {}
        for mote, coordinate in matrix.coordinates.items():
            if coordinate not in cord_dict:
                cord_dict[coordinate] = [mote]
            else:
                cord_dict[coordinate].append(mote)
        #print(cord_dict.items())
        for mote in motes:
            if mote.onMote != -1: #posmatramo samo snifere
                assert len(cord_dict[matrix.coordinates[mote.id]]) > 1

#da li su svi sniferi mds na svim kanalima
    def test_sniffer_mds(self,sim_engine):
        num_channels = 16

        sim_engine = sim_engine(
            diff_config = {
                'exec_numMotes'                 : 50,
                'exec_numSlotframesPerRun'      : 1,
                'conn_class'                    : 'Random',
                'conn_random_init_min_neighbors': 3,
                'phy_numChans'                  : num_channels
            }
        )

        motes  = sim_engine.motes
        matrix = sim_engine.connectivity.matrix
        LINK_PDR_THRESHOLD = sim_engine.settings.sniffer_link_pdr  #promjena
        for channel in d.TSCH_HOPPING_SEQUENCE:
            G = nx.Graph()
            G.add_nodes_from(mote.id for mote in motes) #dodajemo sve nodove

            for i in motes:
                for j in motes:
                    if i != j and i.id < j.id:
                        pdr = matrix.get_pdr(i.id, j.id, channel)
                        if pdr > LINK_PDR_THRESHOLD:
                            G.add_edge(i.id, j.id, weight = pdr)
            S = nx.Graph()
            S.add_nodes_from(mote.id for mote in motes if mote.onMote != -1) #dodajemo samo snifere u graf

            assert  nx.is_dominating_set(G,S) == True



    def test_check_connections(self, sim_engine):

        #TRACE_FILE_PATH = os.path.join( os.path.dirname(__file__),'../traces/grenoble.k7.gz')
        num_channels = 16

        sim_engine = sim_engine(
            diff_config = {
                'exec_numMotes'                 : 50,
                'exec_numSlotframesPerRun'      : 1,
                'conn_class'                    : 'Random',
                #'conn_trace'                    : TRACE_FILE_PATH,
                'conn_random_init_min_neighbors': 3,
                'phy_numChans'                  : num_channels
            }
        )
        #da li je svaki uredjaj povezan sa jednim od snifera sa linkom vecim od 0.9

        LINK_PDR_THRESHOLD = sim_engine.settings.sniffer_link_pdr
        motes  = sim_engine.motes
        matrix = sim_engine.connectivity.matrix

        for mote in motes:
            if mote.onMote == -1: #gledamo samo uredjaje
                snif_pdr = []
                for node in motes:
                    if node.onMote != -1: #samo snifere
                        for channel in d.TSCH_HOPPING_SEQUENCE[:num_channels]:
                            pdr = matrix.get_pdr(mote.id,
                                                  node.id,
                                                  channel)
                            #print(mote.id,mote.onMote,node.id,pdr)
                            snif_pdr.append(pdr)
                            #print(snif_pdr)

                for x in snif_pdr:
                    if x >=  LINK_PDR_THRESHOLD:
                        its_okey = True

                assert its_okey == True



class TestK7 (object):
    def test_sniffer_links_K7(self, sim_engine):

            TRACE_FILE_PATH = os.path.join( os.path.dirname(__file__),'../traces/grenoble.k7.gz')
            num_channels = 16

            sim_engine = sim_engine(
                diff_config = {
                    'exec_numMotes'                 : 50,
                    'exec_numSlotframesPerRun'      : 1,
                    'conn_class'                    : 'K7',
                    'conn_trace'                    : TRACE_FILE_PATH,
                    'conn_random_init_min_neighbors': 3,
                    'phy_numChans'                  : num_channels
                }
            )

            motes  = sim_engine.motes
            matrix = sim_engine.connectivity.matrix

            for mote in motes:
                if mote.onMote != -1: #posmatramo iz ugla snifera
                    for node in motes:
                        if mote.onMote != node.id and node.onMote == -1:
                            #nije snifer, nije uredjaj na kojem se snifer nalazi

                            for channel in d.TSCH_HOPPING_SEQUENCE[:num_channels]:
                                assert matrix._matrix[mote.id][node.id][channel] == matrix._matrix[mote.onMote][node.id][channel]
                                assert matrix._matrix[node.id][mote.id][channel] == matrix._matrix[node.id][mote.onMote][channel]

                        elif mote.onMote == node.id:
                            #uredjaj na kojem se snifer nalazi
                            for channel in d.TSCH_HOPPING_SEQUENCE[:num_channels]:
                                assert matrix.get_pdr(mote.id,
                                                      node.id,
                                                      channel) > 0.9
                                assert matrix.get_pdr(node.id,
                                                      mote.id,
                                                      channel) > 0.9
                                #rssi
                                assert matrix.get_rssi(mote.id,
                                                      node.id,
                                                      channel) > -88
                                assert matrix.get_rssi(node.id,
                                                      mote.id,
                                                      channel) > -88
                        elif node.onMote != -1:
                            #sniferi (on isti, drugi sniferi)
                            for channel in d.TSCH_HOPPING_SEQUENCE[:num_channels]:
                                assert matrix._matrix[mote.id][node.id][channel] == {u'pdr': 0, u'rssi': -1000}
