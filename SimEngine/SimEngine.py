"""
\brief Discrete-event simulation engine.
"""
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

# ========================== imports =========================================

from builtins import zip
from builtins import str
from builtins import range
from past.utils import old_div
from collections import OrderedDict
import hashlib
import platform
import random
import sys
import threading
import time
import traceback
import json
import networkx as nx

from . import Mote
from .Mote import MoteDefines as d
from . import SimSettings
from . import SimLog
from . import Connectivity
from . import SimConfig
from networkx.algorithms.approximation import dominating_set
from itertools import combinations
from itertools import combinations_with_replacement


# =========================== defines =========================================

# =========================== body ============================================

class DiscreteEventEngine(threading.Thread):
    # ===== start singleton
    _instance = None
    _init = False

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(DiscreteEventEngine, cls).__new__(cls)
        return cls._instance

    # ===== end singleton

    def __init__(self, cpuID=None, run_id=None, verbose=False):

        # ===== singleton
        cls = type(self)
        if cls._init:
            return
        cls._init = True
        # ===== singleton

        try:
            # store params
            self.cpuID = cpuID
            self.run_id = run_id
            self.verbose = verbose

            # local variables
            self.dataLock = threading.RLock()
            self.pauseSem = threading.Semaphore(0)
            self.simPaused = False
            self.goOn = True
            self.asn = 0
            self.exc = None
            self.events = {}
            self.uniqueTagSchedule = {}
            self.random_seed = None
            self._init_additional_local_variables()

            # initialize parent class
            threading.Thread.__init__(self)
            self.name = u'DiscreteEventEngine'
        except:
            # an exception happened when initializing the instance

            # destroy the singleton
            cls._instance = None
            cls._init = False
            raise

    def destroy(self):
        cls = type(self)
        if cls._init:
            # initialization finished without exception

            if self.is_alive():
                # thread is start'ed
                self.play()  # cause one more loop in thread
                self._actionEndSim()  # causes self.gOn to be set to False
                self.join()  # wait until thread is dead
            else:
                # thread NOT start'ed yet, or crashed

                # destroy the singleton
                cls._instance = None
                cls._init = False
        else:
            # initialization failed
            pass  # do nothing, singleton already destroyed

    # ======================== thread ==========================================

    def run(self):
        """ loop through events """
        try:
            # additional routine
            self._routine_thread_started()

            # consume events until self.goOn is False
            while self.goOn:

                with self.dataLock:

                    # abort simulation when no more events
                    if not self.events:
                        break

                    # update the current ASN
                    self.asn += 1

                    if self.asn not in self.events:
                        continue

                    intraSlotOrderKeys = list(self.events[self.asn].keys())
                    intraSlotOrderKeys.sort()

                    cbs = []
                    for intraSlotOrder in intraSlotOrderKeys:
                        for uniqueTag, cb in list(self.events[self.asn][intraSlotOrder].items()):
                            cbs += [cb]
                            del self.uniqueTagSchedule[uniqueTag]
                    del self.events[self.asn]

                # call the callbacks (outside the dataLock)
                for cb in cbs:
                    cb()

        except Exception as e:
            # thread crashed

            # record the exception
            self.exc = e

            # additional routine
            self._routine_thread_crashed()

            # print
            output = []
            output += [u'']
            output += [u'==============================']
            output += [u'']
            output += [u'CRASH in {0}!'.format(self.name)]
            output += [u'']
            output += [traceback.format_exc()]
            output += [u'==============================']
            output += [u'']
            output += [u'The current ASN is {0}'.format(self.asn)]
            output += [u'The log file is {0}'.format(
                self.settings.getOutputFile()
            )]
            output += [u'']
            output += [u'==============================']
            output += [u'config.json to reproduce:']
            output += [u'']
            output += [u'']
            output = u'\n'.join(output)
            output += json.dumps(
                SimConfig.SimConfig.generate_config(
                    settings_dict=self.settings.__dict__,
                    random_seed=self.random_seed
                ),
                indent=4
            )
            output += u'\n\n==============================\n'

            sys.stderr.write(output)

            # flush all the buffered log data
            SimLog.SimLog().flush()

        else:
            # thread ended (gracefully)

            # no exception
            self.exc = None

            # additional routine
            self._routine_thread_ended()

        finally:

            # destroy this singleton
            cls = type(self)
            cls._instance = None
            cls._init = False

    def join(self):
        super(DiscreteEventEngine, self).join()
        if self.exc:
            raise self.exc

    # ======================== public ==========================================

    # === getters/setters

    def getAsn(self):
        return self.asn

    def get_mote_by_mac_addr(self, mac_addr):
        for mote in self.motes:
            if mote.is_my_mac_addr(mac_addr):
                return mote
        return None

    # === scheduling

    def scheduleAtAsn(self, asn, cb, uniqueTag, intraSlotOrder):
        """
        Schedule an event at a particular ASN in the future.
        Also removed all future events with the same uniqueTag.
        """

        # make sure we are scheduling in the future
        assert asn > self.asn

        # remove all events with same uniqueTag (the event will be rescheduled)
        self.removeFutureEvent(uniqueTag)

        with self.dataLock:

            if asn not in self.events:
                self.events[asn] = {
                    intraSlotOrder: OrderedDict([(uniqueTag, cb)])
                }

            elif intraSlotOrder not in self.events[asn]:
                self.events[asn][intraSlotOrder] = (
                    OrderedDict([(uniqueTag, cb)])
                )

            elif uniqueTag not in self.events[asn][intraSlotOrder]:
                self.events[asn][intraSlotOrder][uniqueTag] = cb

            else:
                self.events[asn][intraSlotOrder][uniqueTag] = cb

            self.uniqueTagSchedule[uniqueTag] = (asn, intraSlotOrder)

    def scheduleIn(self, delay, cb, uniqueTag, intraSlotOrder):
        """
        Schedule an event 'delay' seconds into the future.
        Also removed all future events with the same uniqueTag.
        """

        with self.dataLock:
            asn = int(self.asn + (float(delay) / float(self.settings.tsch_slotDuration)))

            self.scheduleAtAsn(asn, cb, uniqueTag, intraSlotOrder)

    # === play/pause

    def play(self):
        self._actionResumeSim()

    def pauseAtAsn(self, asn):
        self.scheduleAtAsn(
            asn=asn,
            cb=self._actionPauseSim,
            uniqueTag=(u'DiscreteEventEngine', u'_actionPauseSim'),
            intraSlotOrder=Mote.MoteDefines.INTRASLOTORDER_ADMINTASKS,
        )

    # === misc

    def is_scheduled(self, uniqueTag):
        with self.dataLock:
            return uniqueTag in self.uniqueTagSchedule

    def removeFutureEvent(self, uniqueTag):
        with self.dataLock:
            if uniqueTag not in self.uniqueTagSchedule:
                # new event, not need to delete old instances
                return

            # get old instances occurences
            (asn, intraSlotOrder) = self.uniqueTagSchedule[uniqueTag]

            # make sure it's in the future
            assert asn >= self.asn

            # delete it
            del self.uniqueTagSchedule[uniqueTag]
            del self.events[asn][intraSlotOrder][uniqueTag]

            # and cleanup event structure if it's empty
            if not self.events[asn][intraSlotOrder]:
                del self.events[asn][intraSlotOrder]

            if not self.events[asn]:
                del self.events[asn]

    def terminateSimulation(self, delay):
        with self.dataLock:
            self.asnEndExperiment = self.asn + delay
            self.scheduleAtAsn(
                asn=self.asn + delay,
                cb=self._actionEndSim,
                uniqueTag=(u'DiscreteEventEngine', u'_actionEndSim'),
                intraSlotOrder=Mote.MoteDefines.INTRASLOTORDER_ADMINTASKS,
            )

    # ======================== private ========================================

    def _actionPauseSim(self):
        assert self.simPaused == False
        self.simPaused = True
        self.pauseSem.acquire()

    def _actionResumeSim(self):
        if self.simPaused:
            self.simPaused = False
            self.pauseSem.release()

    def _actionEndSim(self):
        with self.dataLock:
            self.goOn = False

    def _actionEndSlotframe(self):
        """Called at each end of slotframe_iteration."""

        slotframe_iteration = int(old_div(self.asn, self.settings.tsch_slotframeLength))

        # print
        if self.verbose:
            print(u'   slotframe_iteration: {0}/{1}'.format(slotframe_iteration,
                                                            self.settings.exec_numSlotframesPerRun - 1))

        # schedule next statistics collection
        self.scheduleAtAsn(
            asn=self.asn + self.settings.tsch_slotframeLength,
            cb=self._actionEndSlotframe,
            uniqueTag=(u'DiscreteEventEngine', u'_actionEndSlotframe'),
            intraSlotOrder=Mote.MoteDefines.INTRASLOTORDER_ADMINTASKS,
        )

    # ======================== abstract =======================================

    def _init_additional_local_variables(self):
        pass

    def _routine_thread_started(self):
        pass

    def _routine_thread_crashed(self):
        pass

    def _routine_thread_ended(self):
        pass


class SimEngine(DiscreteEventEngine):
    DAGROOT_ID = 0

    def _init_additional_local_variables(self):
        self.settings = SimSettings.SimSettings()

        # set random seed
        if self.settings.exec_randomSeed == u'random':
            self.random_seed = random.randint(0, sys.maxsize)
        elif self.settings.exec_randomSeed == u'context':
            # with context for exec_randomSeed, an MD5 value of
            # 'startTime-hostname-run_id' is used for a random seed
            startTime = SimConfig.SimConfig.get_startTime()
            if startTime is None:
                startTime = time.time()
            context = (platform.uname()[1], str(startTime), str(self.run_id))
            md5 = hashlib.md5()
            md5.update(u'-'.join(context).encode('utf-8'))
            self.random_seed = int(md5.hexdigest(), 16) % sys.maxsize
        else:
            assert isinstance(self.settings.exec_randomSeed, int)
            self.random_seed = self.settings.exec_randomSeed
        # apply the random seed; log the seed after self.log is initialized
        random.seed(a=self.random_seed)

        if self.settings.motes_eui64:
            eui64_table = self.settings.motes_eui64[:]
            if len(eui64_table) < self.settings.exec_numMotes:
                eui64_table.extend(
                    [None] * (self.settings.exec_numMotes - len(eui64_table))
                )
        else:
            eui64_table = [None] * self.settings.exec_numMotes

        self.motes = [
            Mote.Mote.Mote(id, eui64)
            for id, eui64 in zip(
                list(range(self.settings.exec_numMotes)),
                eui64_table
            )
        ]

        eui64_list = set([mote.get_mac_addr() for mote in self.motes])
        if len(eui64_list) != len(self.motes):
            assert len(eui64_list) < len(self.motes)
            raise ValueError(u'given motes_eui64 causes dulicates')

        self.connectivity = Connectivity.Connectivity(self)
        self.log = SimLog.SimLog().log
        SimLog.SimLog().set_simengine(self)

        # log the random seed
        self.log(
            SimLog.LOG_SIMULATOR_RANDOM_SEED,
            {
                u'value': self.random_seed
            }
        )
        # flush buffered logs, which are supposed to be 'config' and
        # 'random_seed' lines, right now. This could help, for instance, when a
        # simulation is stuck by an infinite loop without writing these
        # 'config' and 'random_seed' to a log file.
        SimLog.SimLog().flush()

        # find best sniffer locations and
        # add it in the network
        if self.settings.sniffer_deploy:
            if self.settings.selection_algorithm == 'Graph':
                # TELFOR 2020 Algoritam
                # algorithm to select sniffers that cover the deployment
                LINK_PDR_THRESHOLD = self.settings.sniffer_link_pdr
                sniffers_locations = set()
                channel_graphs = {}
                for channel in d.TSCH_HOPPING_SEQUENCE:
                    G = nx.Graph()
                    G.add_nodes_from(mote.id for mote in self.motes)
                    for i in self.motes:
                        for j in self.motes:
                            if i != j and i.id < j.id:
                                pdr = self.connectivity.get_pdr(i.id, j.id, channel)
                                if pdr >= LINK_PDR_THRESHOLD:
                                    G.add_edge(i.id, j.id, weight=pdr)

                    minDominatingSet = dominating_set.min_weighted_dominating_set(G)
                    sniffers_locations |= minDominatingSet
                    # store graph at every channel for further analysis
                    channel_graphs[channel] = G

                # ordering sniffers by pdr sum
                sum_pdr = {}
                for snif_loc in sniffers_locations:
                    sum_pdr[snif_loc] = 0
                    for mote in self.motes:
                        if snif_loc != mote.id:
                            for channel in d.TSCH_HOPPING_SEQUENCE:
                                sum_pdr[snif_loc] = sum_pdr[snif_loc] + self.connectivity.get_pdr(snif_loc, mote.id,
                                                                                                  channel)
                ordered_sniffers_locations = sorted(sum_pdr.keys(), key=sum_pdr.get)

                # removing sniffers
                sniffers_g = nx.Graph()
                for node in sniffers_locations:
                    sniffers_g.add_node(node)

                if len(sniffers_g.nodes()) > len(sniffers_locations) * (1 - self.settings.removal_load):
                    for node in ordered_sniffers_locations:
                        sniffers_g.remove_node(node)
                        # check new graph at every channel
                        for channel in d.TSCH_HOPPING_SEQUENCE:
                            channel_graph = channel_graphs[channel]
                            if not nx.is_dominating_set(channel_graph, sniffers_g):
                                sniffers_g.add_node(node)
                        if len(sniffers_g.nodes()) <= len(sniffers_locations) * (1 - self.settings.removal_load):
                            break

                sniffers_locations = []
                for sniffer in sniffers_g.nodes():
                    sniffers_locations.append(sniffer)

                # calculate average pdr
                pdr_sum_comb = 0
                for mote in self.motes:
                    pdr_sum_mote = 0
                    # idemo kroz sve kanale
                    for channel in d.TSCH_HOPPING_SEQUENCE:
                        p_err = 1
                        # idemo kroz sve snifere iz kombinacije
                        for sniffer in sniffers_locations:
                            if mote.id != sniffer:
                                p_err = p_err * (1 - self.connectivity.get_pdr(mote.id, sniffer, channel))
                            else:
                                # snifer je na motu, vjerovatnoca greske je u tom slucaju 0
                                p_err = 0
                                break

                        # vjerovatnoca uspjesnog prenosa paketa sa tog senzora, na tom kanalu ka svim sniferima
                        p_success = 1 - p_err
                        pdr_sum_mote = pdr_sum_mote + p_success

                    avg_pdr_mote = pdr_sum_mote / len(d.TSCH_HOPPING_SEQUENCE)
                    pdr_sum_comb = pdr_sum_comb + avg_pdr_mote

                avg_pdr_comb = pdr_sum_comb / self.settings.exec_numMotes
                average_pdr = avg_pdr_comb


            elif self.settings.selection_algorithm == 'Probabilistic':

                assert self.settings.sniffer_num == 0 or self.settings.target_pdr == 0
                assert self.settings.sniffer_num != 0 or self.settings.target_pdr != 0

                sensors = []
                for mote in self.motes:
                    sensors.append(mote.id)
                best_comb = {'sniffers_locations': [], 'average_pdr': 0}

                # idemo na osnovu dostupnih snifera
                if self.settings.sniffer_num > 0:

                    number_of_sniffers = int(self.settings.sniffer_num)

                    if self.settings.comb_with_repetitions:
                        all_combs = combinations_with_replacement(sensors, number_of_sniffers)
                    else:
                        all_combs = combinations(sensors, number_of_sniffers)

                    for combination in all_combs:
                        sniffers = list(combination)
                        pdr_sum_comb = 0
                        # idemo da izracunamo pdr_sum za sve motove za datu kombinaciju
                        for mote in self.motes:
                            pdr_sum_mote = 0
                            # idemo kroz sve kanale
                            for channel in d.TSCH_HOPPING_SEQUENCE:
                                p_err = 1
                                # idemo kroz sve snifere iz kombinacije
                                for sniffer in sniffers:
                                    if mote.id != sniffer:
                                        p_err = p_err * (1 - self.connectivity.get_pdr(mote.id, sniffer, channel))
                                    else:
                                        # snifer je na motu, vjerovatnoca greske je u tom slucaju 0
                                        p_err = 0
                                        break

                                # vjerovatnoca uspjesnog prenosa paketa sa tog senzora, na tom kanalu ka svim sniferima
                                p_success = 1 - p_err
                                pdr_sum_mote = pdr_sum_mote + p_success

                            avg_pdr_mote = pdr_sum_mote / len(d.TSCH_HOPPING_SEQUENCE)
                            pdr_sum_comb = pdr_sum_comb + avg_pdr_mote

                        avg_pdr_comb = pdr_sum_comb / self.settings.exec_numMotes
                        if avg_pdr_comb > best_comb['average_pdr']:
                            best_comb['sniffers_locations'] = sniffers
                            best_comb['average_pdr'] = avg_pdr_comb

                    sniffers_locations = best_comb['sniffers_locations']
                    average_pdr = best_comb['average_pdr']
                # trazimo target pdr
                elif self.settings.target_pdr > 0:
                    # rijesiti sa dodatnom funkcijom trenutno ovo ne radi jer break nije dje mu je i mjesto
                    target_pdr = self.settings.target_pdr
                    all_sniffers_num = list(range(1, self.settings.exec_numMotes + 1))

                    for sniffers_num in all_sniffers_num:

                        if self.settings.comb_with_repetitions:
                            all_combs = combinations_with_replacement(sensors, sniffers_num)
                        else:
                            all_combs = combinations(sensors, sniffers_num)

                        for combination in all_combs:
                            sniffers = list(combination)
                            pdr_sum_comb = 0
                            # idemo da izracunamo pdr_sum za sve motove za datu kombinaciju
                            for mote in self.motes:
                                pdr_sum_mote = 0
                                # idemo kroz sve kanale
                                for channel in d.TSCH_HOPPING_SEQUENCE:
                                    p_err = 1
                                    # idemo kroz sve snifere iz kombinacije
                                    for sniffer in sniffers:
                                        if mote.id != sniffer:
                                            p_err = p_err * (1 - self.connectivity.get_pdr(mote.id, sniffer, channel))
                                        else:
                                            # snifer je na motu, vjerovatnoca greske je u tom slucaju 0
                                            p_err = 0
                                            break

                                    # vjerovatnoca uspjesnog prenosa paketa sa tog senzora, na tom kanalu ka svim sniferima
                                    p_success = 1 - p_err
                                    pdr_sum_mote = pdr_sum_mote + p_success

                                avg_pdr_mote = pdr_sum_mote / len(d.TSCH_HOPPING_SEQUENCE)
                                pdr_sum_comb = pdr_sum_comb + avg_pdr_mote

                            avg_pdr_comb = pdr_sum_comb / self.settings.exec_numMotes
                            if avg_pdr_comb >= target_pdr:
                                best_comb['sniffers_locations'] = sniffers
                                best_comb['average_pdr'] = avg_pdr_comb
                                break
                        if best_comb['sniffers_locations']:
                            break

                    sniffers_locations = best_comb['sniffers_locations']
                    average_pdr = best_comb['average_pdr']

            # unijeti sniferi
            elif self.settings.selection_algorithm == 'Exact':

                sniffers_locations = self.settings.exact_sniffers
                average_pdr = 0

            self.log(
                SimLog.LOG_SNIFFERS_LOCATIONS_AND_PDR,
                {
                    u'sniffers_locations': sniffers_locations,
                    u'average_pdr': average_pdr,
                }
            )

            # add sniffers in the network
            eui64_sniffers_table = [None] * len(sniffers_locations)

            self.motes = self.motes + [
                Mote.Mote.Mote(id, eui64)
                for id, eui64 in zip(
                    list(range(self.settings.exec_numMotes, self.settings.exec_numMotes + len(sniffers_locations))),
                    eui64_sniffers_table
                )
            ]

            eui64_list = set([mote.get_mac_addr() for mote in self.motes])
            if len(eui64_list) != len(self.motes):
                assert len(eui64_list) < len(self.motes)
                raise ValueError(u'given motes_eui64 causes dulicates')

            # new motes ID for sniffers
            sniffers_IDs = [ids for ids in range(self.settings.exec_numMotes,
                                                 self.settings.exec_numMotes + len(sniffers_locations))]

            for target_mote_id, sniffer_id in zip(sniffers_locations, sniffers_IDs):
                # Mote na cijoj se lokaciji nalazi snifer
                self.motes[sniffer_id].onMote = target_mote_id

            Connectivity.Connectivity().add_sniffers(sniffers_ids=sniffers_IDs)

            # once sniffers have been selected, configure them
            for sniffer_id in sniffers_IDs:
                self.motes[sniffer_id].setSniffer()

                # log each individual sniffer
                self.log(
                    SimLog.LOG_SNIFFER_INIT,
                    {
                        u'_mote_id': sniffer_id,
                    }
                )

        # select dagRoot
        self.motes[self.DAGROOT_ID].setDagRoot()

        # boot all motes
        for i in range(len(self.motes)):
            self.motes[i].boot()

    def _routine_thread_started(self):
        # log
        self.log(
            SimLog.LOG_SIMULATOR_STATE,
            {
                u'name': self.name,
                u'state': u'started'
            }
        )

        # schedule end of simulation
        self.scheduleAtAsn(
            asn=self.settings.tsch_slotframeLength * self.settings.exec_numSlotframesPerRun,
            cb=self._actionEndSim,
            uniqueTag=(u'SimEngine', u'_actionEndSim'),
            intraSlotOrder=Mote.MoteDefines.INTRASLOTORDER_ADMINTASKS,
        )

        # schedule action at every end of slotframe_iteration
        self.scheduleAtAsn(
            asn=self.asn + self.settings.tsch_slotframeLength - 1,
            cb=self._actionEndSlotframe,
            uniqueTag=(u'SimEngine', u'_actionEndSlotframe'),
            intraSlotOrder=Mote.MoteDefines.INTRASLOTORDER_ADMINTASKS,
        )

    def _routine_thread_crashed(self):
        # log
        self.log(
            SimLog.LOG_SIMULATOR_STATE,
            {
                "name": self.name,
                "state": "crash"
            }
        )

    def _routine_thread_ended(self):
        # log
        self.log(
            SimLog.LOG_SIMULATOR_STATE,
            {
                "name": self.name,
                "state": "stopped"
            }
        )
